function fizzbuzz(maxValue){
    let retString = '';
    for(let i = 1; i <= maxValue; i ++){
        if(i % 2 == 0)
            retString = retString.concat("Fizz");
        if(i % 3 == 0)
            retString = retString.concat("Buzz");
        if(i % 2 != 0 && i % 3 != 0)
            retString = retString.concat(i);
        retString = retString.concat(", ");
    }
    return retString;
}
function fizzbuzzprime(maxValue){
    let retString = '', prime = true;
    for(let i = 1; i <= maxValue; i ++, prime = true){
        if(i % 2 == 0)
            retString = retString.concat("Fizz");
        if(i % 3 == 0)
            retString = retString.concat("Buzz");
        if(i % 2 != 0 && i % 3 != 0)
            retString = retString.concat(i);
        for(let j = 1; j < i; j ++){
            if(i % j == 0 && j != 1)
                prime = false;
        }
        if(prime && i != 1)
            retString = retString.concat("Prime");
        retString = retString.concat(", ");
    }
    return retString;
}